# Docker Traefik Wordpress

This guide will explain how to setup Traefik, a reverse proxy, in a docker container with SSL certs.

## Files in this repo
### For Traefik
**docker-compose.yml**
- This is the docker compose file that will run Traefik. You probably won't need to touch this.

**traefik.yml**
- A Traefik config file. Again, you probably won't need to touch this.

**dynamic-conf.yml**
- Same as above.

### To be copied to other projects as needed
**wordpress.yml**
- This is the docker file for a single WordPress site.

**wordpress-multi.yml**
- This is the docker file for a WordPress multisite.

**.env.example**
- This is the .env file for the 2 wordpress docker files above.

## Clone this repo

Clone this repo where ever you normally store your projects.

## Generate Certs

1. Install mkcert and nss

```sh
brew install mkcert nss
```

2. Install the certs

``` sh
mkcert -install
```

3. In the same directory as the Traefik files create a directory called `certs`

``` sh
mkdir certs
```

4. Generate the certs

``` sh
mkcert -cert-file certs/local-cert.pem -key-file certs/local-key.pem "app.localhost" "*.app.localhost"
```

## Run the Traefik container

1. Create the docker network which will allow traefik to talk to other containers

``` sh
docker network create traefik
```

2. In the same folder run the following command

``` sh
docker-compose up -d
```

This will run traefik, and we can navigate to [https://app.localhost](https://app.localhost) to check if it's working

## Creating a WordPress server

1. Create a new directory where the project docker files will live. You'll want to do this where ever you normally keep your projects. For this example we'll create one called `wordpress-docker`
 
``` sh
mkdir wordpress-docker
```

2. If cloning an existing repo, clone that inside your new directory so you'll have this structure.

``` sh
/some/path/wordpress-docker/cloned-repo
```

3. Copy the files needed into our new directory `wordpress-docker`.

### If creating a single site run

``` sh
cp ../traefik/wordpress.yml .env.example ./
```

### If creating a multisite run

``` sh
cp ../traefik/wordpress-multisite.yml .env.example ./
```

4. Rename the new files

``` sh
mv .env.example .env
```

and

``` sh
mv wordpress.yml docker-compose.yml
```

or

``` sh
mv wordpress-multisite.yml docker-compose.yml
```

5. Edit the `.env` file.

``` sh
# Choose a good name for the project, this will be used in the url 
PROJECT=name

# The DB creds don't really matter since your local server isn't internet facing. So I usually just leave as is below.
DB_NAME=wallop
DB_USER=wallop
DB_PASS=1234

# This is the name of the folder where the repo lives
REPO_NAME=name-of-folder

# If cloning a db, sometimes the prefix is different from wp_
TABLE_PREFIX=wp_

# Mysql Version. Generally this is 5.7
MYSQL_VERSION=5.7

# WordPress Version. A list of options can be found here
WORDPRESS_VERSION=latest
```

If you want to read more, [here](https://knplabs.com/en/blog/how-to-handle-https-with-docker-compose-and-mkcert-for-local-development) is the guide I used.

Probably best to move `wordpress.yml` or `wordpress-multisite.yml` where ever you want it. 

Don't forget to create a .env file


